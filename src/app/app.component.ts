import { Component, OnInit } from '@angular/core';
import { PharmacyService } from './pharmacy.service';

@Component({
  selector: 'app-root',
  styles: [`
    agm-map {
      height: 300px;
    }
  `],
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})


export class AppComponent implements OnInit {
  local = "";
  comuna = "";
  error = "";
  locals: any[] = [];
  constructor(
    protected userService: PharmacyService
  ) {
  }

  search(){
    console.log(this.local)
    console.log(this.comuna)
    this.userService.getPharmacy(this.local, this.comuna)
    .subscribe(
      (data) => { // Success
        console.log(data)
        this.locals = data['data'];
        if(data['data']==null){
          this.error = data['description'];
        }else{
          this.error = "";
        }
      },
      (error) => {
        console.error(error);
      }
    );
  }

  ngOnInit() {
    
  }
}
