import { BrowserModule } from '@angular/platform-browser';
import { NgModule , Component} from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AgmCoreModule } from '@agm/core';
import { AppRoutingModule } from './app-routing.module';
import { PharmacyService } from './pharmacy.service';
import { AppComponent } from './app.component';
import { HttpClientModule} from '@angular/common/http';


@NgModule({
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBVOenUCA5njtzHQodR2JLXbo4KwZw0cMk'
    })
  ],
  providers: [PharmacyService],
  declarations: [ AppComponent ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
