import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PharmacyService {

  constructor(protected http: HttpClient) { }

  getPharmacy(local, comuna) {
    return this.http.get('http://localhost:8888/api/pharmacy?comuna='+comuna+'&local='+local);
  }
}
